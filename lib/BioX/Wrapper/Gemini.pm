package BioX::Wrapper::Gemini;

use strict;
use 5.008_005;
our $VERSION = '0.01';

1;
__END__

=encoding utf-8

=head1 NAME

BioX::Wrapper::Gemini - Blah blah blah

=head1 SYNOPSIS

  use BioX::Wrapper::Gemini;

=head1 DESCRIPTION

BioX::Wrapper::Gemini is

=head1 AUTHOR

Jillian Rowe E<lt>jillian.e.rowe@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2015- Jillian Rowe

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

=cut
